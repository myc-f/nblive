## virtualBox 挂载宝塔开发
```
mount -t vboxsf think-swoole /mnt/www
cd /mnt/www/nblive
php think swoole:server

宝塔自启动服务

echo "/www/server/php/71/bin/php /webdir/think swoole:server" >> /etc/rc.d/rc.local

端口查看 netstat -anp|grep 9501

```
## ThinkPHP 安装

```
composer create-project topthink/think=5.1.* tp5
cd tp5
composer require topthink/think-swoole:2.*
```

## REDIS 设计 

### 店铺用户

shop_{$shop_id}_user_fd list sadd fd

### 店铺客服

shop_{$shop_id}_kf_fd" sadd $fd

### 用户基本信息

chat_user_info_[uid] {userinfo}
用户连接店铺客服FD列表,多终端
chat_u1f_sid:[shop_id]_uid:[user_id]_kid:[kf_id]_fd:[fd] {userinfo} 在线列表
chat_u2f_sid:[shop_id]_uid:[user_id]_kid:[kf_id]_fd:[fd] {fd} 通讯FD

### 客服基本信息

chat_kf_info_[uid] {userinfo}
客服连接用户FD列表,多终端
chat_k1f_sid:[shop_id]_uid:[user_id]_fd:[fd] {userinfo} 在线列表
chat_k2f_sid:[shop_id]_uid:[user_id]_fd:[fd] {fd} 通讯FD

