<?php
namespace app\kefu;

use app\common\controller\Common;

class ServicefInit extends Common
{
    public function __construct()
    {
        parent::__construct();
        if(!session('?kefu')){
            $this->redirect(url('login/index'));
        }
        $this->kefu = session('kefu');
        $this->assign('socket', env('live.chat'));
        $this->assign('kefu', $this->kefu);
    }
}