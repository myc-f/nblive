<?php
namespace app\kefu\validate;

use think\Validate;

/**
 * 用户验证器
 * @package app\system\validate
 */
class KfShop extends Validate
{
    //定义验证规则
    protected $rule = [
        'name|店名'         => 'require',
        'avatar|头像'       => 'requireWith:role_id|notIn:0,1',
        'password|密码'     => 'require|length:32|confirm',
        'username|用户名'   => 'require|alphaNum|unique:kf_shop,username',
        //'code|唯一码'       => 'require|unique:kf_shop,code',
        '__token__'        => 'require|token',
    ];

    //定义验证提示
    protected $message = [
        'code.require'  => '唯一码不能为空',
        'code.unique'   => '唯一码已存在',
    ];

    // 自定义更新场景
    public function sceneUpdate()
    {
        return $this->only(['name', 'username',  'password', '__token__'])
                    ->remove('password', ['require'])
                    ->append('password', ['requireWith']);
    }

    

}
