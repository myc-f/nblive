<?php
namespace app\kefu\validate;

use think\Validate;

/**
 * 用户验证器
 * @package app\system\validate
 */
class KfUser extends Validate
{
    //定义验证规则
    protected $rule = [
        'username|用户名'   => 'require',
        'avatar|头像'       => 'require',
        'sort|优先级别'       => 'require|number|between:1,120',
        '__token__'        => 'require|token',
    ];

    //定义验证提示
    protected $message = [
        '__token__'  => '非法操作！',
    ];

    // 自定义更新场景
    public function sceneInsert()
    {
        return $this->only(['username', 'avatar', '__token__']);                   
    }

    

}
