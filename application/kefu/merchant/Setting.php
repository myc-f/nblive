<?php
namespace app\kefu\merchant;

use \think\facade\Url;
use app\kefu\model\KfShop as KfShopModel;

use app\kefu\MerchantInit;

class Setting extends MerchantInit
{

    /**
     * 基础设置
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            $data = input();
           
            // 验证
            $result = $this->validate($data, 'KfShop.update');
            if(true !==  $result) {
                return $this->error($result);
            }
            KfShopModel::update($data);
            return $this->success('更新成功');
        }
        $map = ['id'=>$this->shop['id']];
        $formData = KfShopModel::where($map)->find();
        $this->assign('formData',$formData);
        return $this->fetch('merchant/setting');
    }
    public function help(){
        Url::root('/index.php');
        $url = url('//kefu/code/index',['code'=>$this->shop['code']],'',true);
        $this->assign('url',$url);
        return $this->fetch('merchant/help');
    }
}
