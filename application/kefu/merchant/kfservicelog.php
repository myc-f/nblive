<?php
namespace app\kefu\merchant;

use app\kefu\model\kfServiceLog as kfServiceLogModel;

use app\kefu\MerchantInit;

class kfservicelog extends MerchantInit
{

    private $password = 'abcd1234';
    public function index()
    {
        $password = $this->password;//默认密码
        if ($this->request->isAjax()) {
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $where[]    = ['shop_id', 'eq', $this->shop['id']];
            if ($keyword) {
                $where[] = ['username', 'like', "%{$keyword}%"];
            }

            $data['data'] = kfServiceLogModel::where($where)->page($page)->limit($limit)->order('id DESC')->select();
            $data['count'] = kfServiceLogModel::where($where)->count('id');
            $data['code'] = 0;
            $data['msg'] = '';
            return json($data);
        }

        $assign = [];

        $assign['password'] = $password;

        return $this->assign($assign)->fetch('merchant/kfservicelog/index');
    }
    
}
