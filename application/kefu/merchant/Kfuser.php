<?php
namespace app\kefu\merchant;

use app\kefu\model\KfUser as KfUserModel;

use app\kefu\MerchantInit;

class Kfuser extends MerchantInit
{

    private $password = 'abcd1234';
    public function index()
    {
        $password = $this->password;//默认密码
        if ($this->request->isAjax()) {
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $where[]    = ['shop_id', 'eq', $this->shop['id']];
            if ($keyword) {
                $where[] = ['username', 'like', "%{$keyword}%"];
            }

            $data['data'] = KfUserModel::where($where)->page($page)->limit($limit)->order('id DESC')->select();
            $data['count'] = KfUserModel::where($where)->count('id');
            $data['code'] = 0;
            $data['msg'] = '';
            return json($data);
        }

        $assign = [];

        $assign['password'] = $password;

        return $this->assign($assign)->fetch('merchant/kfuser/index');
    }
    /**
     * 新增加用户
     *
     * @return void
     */
    public function addUser()
    {
        $password = $this->password;//默认密码
        if ($this->request->isAjax()) {
            $data = input();
            $data['shop_id']=$this->shop['id'];
            // 验证
            $result = $this->validate($data, 'KfUser');
            if(true !==  $result) {
                return $this->error($result);
            }
            $data['salt'] = random(10,0);
            $data['password'] = md5($password . $data['salt']);
            try {
                $id = KfUserModel::create($data);
            } catch (\Exception $e) {
                return $this->error('已存在相同用户,错误号:'.$e->getCode());
            }
            $this->success("新增成功",url('index'));
        }
        $assign = [
            'formData'=>[]
            ,'title'=>'添加客服[默认密码'.$password.']'
        ];
        return $this->assign($assign)->fetch('merchant/kfuser/edit');
    }
    /**
     * 修改加用户
     *
     * @return void
     */
    public function editUser()
    {
        $id = input('id');
        $password = $this->password;//默认密码
        if ($this->request->isAjax()) {
            $data = input();
            $data['shop_id']=$this->shop['id'];
            // 验证
            $result = $this->validate($data, 'KfUser.update');
            if(true !==  $result) {
                return $this->error($result);
            }
            $map = ['shop_id'=>$this->shop['id'],'id'=>$data['id']];
            $data=['username'=>$data['username'],'avatar'=>$data['avatar'],'sort'=>$data['sort']];
            try {
                $res = KfUserModel::where($map)->update($data);
            } catch (\Exception $e) {
                return $this->error('已存在相同用户,错误号:'.$e->getCode());
            }
            
            $this->success("修改成功",url('index'));
        }
        $map = ['shop_id'=>$this->shop['id'],'id'=>$id];
        $formData = KfUserModel::where($map)->find();
        $assign = [
            'formData'=>$formData
            ,'title'=>'修改客服['.$formData['username'].']'
        ];
        return $this->assign($assign)->fetch('merchant/kfuser/edit');
    }
    /**
     * 删除用户
     *
     * @return void
     */
    public function delUser()
    {
        $id = (array)input('id');
        $map = ['shop_id'=>$this->shop['id']];
        KfUserModel::where($map)->delete($id);
        return $this->success('删除完成!');
    }
     /**
     * 重置密码
     *
     * @return void
     */
    public function resetUser()
    {
        $id = input('id');
        $map = ['shop_id'=>$this->shop['id'],'id'=>$id];
        $data['salt'] = random(10,0);
        $data['password'] = md5($this->password . $data['salt']);
        KfUserModel::where($map)->update($data);
        return $this->success('重置完成,新密码为:'.$this->password);
    }
}
