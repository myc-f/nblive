<?php
namespace app\kefu\merchant;

use app\kefu\model\kfServiceLog as kfServiceLogModel;
use app\kefu\model\KfChatLog as KfChatLogModel;
use app\kefu\model\KfUser as KfUserModel;
use app\kefu\model\KfGuestTrace as KfGuestTraceModel;

use app\kefu\MerchantInit;

class Chart extends MerchantInit
{

    public function index()
    {

        if ($this->request->isAjax()) {
            $cat = input('cat','client_browser');
            $xaxis = $series = [];
            $res = KfGuestTraceModel::field($cat.', count(*) as num')->where(['shop_id'=>$this->shop['id']])->group($cat)->select();
            foreach ($res as $k => $v) {
                array_push($xaxis,$v[$cat]);
                array_push($series,$v['num']);
            }
            return $this->success('','',['xaxis'=>$xaxis,'series'=>$series]);
        }
        $data=[];
        $data['guest_num']= kfServiceLogModel::where(['shop_id'=>$this->shop['id']])->count();
        $data['guest_chat_num']=KfChatLogModel::where(['shop_id'=>$this->shop['id'],'source'=>'guest'])->count();
        $data['kf_num']=KfUserModel::where(['shop_id'=>$this->shop['id']])->count();
        $data['kf_chat_num']=KfChatLogModel::where(['shop_id'=>$this->shop['id'],'source'=>'kf'])->count();

        $this->assign('data',$data);
        return $this->fetch('merchant/chart/index');
    }
    
}
