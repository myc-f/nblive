<?php
namespace app\kefu\merchant;

use app\kefu\model\KfUser as KfUserModel;

use app\kefu\MerchantInit;

class Index extends MerchantInit
{

    public function index()
    {
        if ($this->request->isAjax()) {
            $where      = $data = [];
            $page       = $this->request->param('page/d', 1);
            $limit      = $this->request->param('limit/d', 15);
            $keyword    = $this->request->param('keyword/s');
            $where[]    = ['id', 'neq', 1];
            if ($keyword) {
                $where[] = ['username', 'like', "%{$keyword}%"];
            }

            $data['data'] = KfUserModel::where($where)->page($page)->limit($limit)->select();
            $data['count'] = KfUserModel::where($where)->count('id');
            $data['code'] = 0;
            $data['msg'] = '';
            return json($data);
        }
        return $this->fetch('merchant/index');
    }
}
