<?php
namespace app\kefu\controller;
use app\common\controller\Common;
use app\kefu\model\KfShop as KfShopModel;

class Code extends Common
{
    public function index()
    {
        $shop_code = input('code');
        $this->assign('is_pc',!$this->request->isMobile());
        $this->assign('iframe_url',url('kefu/code/chat',['shop_code'=>$shop_code],'',true));
        $this->assign('uuid',gene_guid());
        $this->assign('socket', env('live.chat'));
        return $this->fetch();
    }
    public function chat()
    {
        $uuid = input('uuid/s', gene_guid());
        $shop_code = input('shop_code');
        $shop = KfShopModel::where(['code'=>$shop_code])->find();
        if(!$shop){
            die('信息错误,请联系管理员');
        }
        $this->assign('socket', env('live.chat'));
        $this->assign('shop',$shop);
        $this->assign('uuid',$uuid);
        return $this->fetch();
    }
}