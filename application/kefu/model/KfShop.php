<?php
namespace app\kefu\model;
use app\kefu\model\KfUser as KfUserModel;
use think\Model;

class KfShop extends Model
{
    protected $auto = ['code'];
    protected function setCodeAttr($value)
    {
        return empty($value) ? gene_guid() : $value;
    }
    public function getKfUserAttr($value,$data)
    {
        return KfUserModel::field('user_pwd',true)->where('status', 1)->where('shop_id',$data['id'])->order('id', 'asc')->select()->toArray();
    }
/**
     * 登录
     * @param string $account 账号
     * @param string $password 密码
     * @param bool $remember 记住登录 TODO
     * @param string $field 登陆之后缓存的字段
     * @return stirng|array
     */
    public static function login($account = '', $password = '', $remember = false, $field = 'id,name,code,username,avatar,expire_time', $token = true)
    {
        $where = $rule = [];
        $account = trim($account);
        $password = $rule['password'] = trim($password);
        $field = trim($field, ',');

        // 匹配登录方式
        if (is_email($account)) {
            // 邮箱登录
            $where['email'] = $rule['email'] = $account;
        } elseif (is_mobile($account)) {
            // 手机号登录
            $where['mobile'] = $rule['mobile'] = $account;
        } elseif (is_username($account)) {
            // 用户名登录
            $where['username'] = $rule['username'] = $account;
        } else {
            exception('登陆账号异常！', 10001);
            return false;
        }

        if ($token !== false) {
            $rule['__token__'] = input('param.__token__') ?: $token;
            $scene = 'loginToken';
        } else {
            $scene = 'login';
        }

        $where['status'] = 1;
        $member = self::where($where)->field(true)->find();
        if (!$member) {
            exception('用户不存在或被禁用！', 10003);
            return false;
        }

        $member = $member->toArray();

        // 密码校验
        if (md5($password . $member['salt']) != $member['password']) {
            exception('登陆密码错误！', 10004);
            return false;
        }

        // 检查有效期
        if ($member['expire_time'] !== 0 && strtotime($member['expire_time']) < time()) {
            exception('账号已过期！', 10005);
            return false;
        }

        $login = [];
        $login['id'] = $member['id'];
        $fields = explode(',', $field);

        foreach ($fields as $v) {
            if ($v == 'password' || $v == 'salt') {
                continue;
            }
            $login[$v] = $member[$v];
        }

        return self::autoLogin($login);
    }
    /**
     * @param bool $oauth 第三方授权登录
     * @return bool|array
     */
    public static function autoLogin($data = [], $oauth = false)
    {
        if ($oauth) {
            $map = [];
            $map['id'] = $data['id'];
            $map['status'] = 1;
            $data = $this->where($map)->field('id,name,username,expire_time,avatar')->find();
            if (!$data) {
                exception('用户不存在或被禁用！', 10006);
                return false;
            }

            $data = $data->toArray();
            // 检查有效期
            if ($data['expire_time'] !== 0 && strtotime($data['expire_time']) < time()) {
                exception('账号已过期！', 10007);
                return false;
            }
        }
        $map = [];
        $map['last_login_ip'] = get_client_ip();
        $map['last_login_time'] = request()->time();
        self::where('id', $data['id'])->update($map);

        session('merchant', $data);
        session('login_merchant_sign', self::dataSign($data));
        // runhook('kfuser_login', $data);

        return $data;
    }
    /**
     * 数据签名认证
     * @param array $data 被认证的数据
     * @return string 签名
     */
    public static function dataSign($data = [])
    {
        if (!is_array($data)) {
            $data = (array) $data;
        }

        ksort($data);
        $code = http_build_query($data);
        $sign = sha1($code);

        return $sign;
    }

}