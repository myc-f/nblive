<?php
namespace app\kefu\model;
use think\Model;
use apanly\BrowserDetector\Browser;
use apanly\BrowserDetector\Device;
use apanly\BrowserDetector\Os;

class KfGuestTrace extends Model
{
    protected $autoWriteTimestamp = true;
    /**
     * 用户上线记录
     */
    public static function addTrace($fd,$data){
        $ua = $data['header']['user-agent'];
        $browser = new Browser($ua);
        $os = new Os($ua);
        $device = new Device($ua);
        $data = [
            'type'                    => $data['type']
            ,'code'                   => $data['code']
            ,'shop_id'                => $data['shop_id']
            ,'fd'                     => $fd
            ,'ua'                     => $data['header']['user-agent']
            ,'ip'                     => $data['server']['remote_addr']
            ,'referer'                => ''
            ,'talk_url'               => ''
            ,'source'                 => $data['source']
            ,'client_browser'         => $browser->getName() ?? ''
            ,'client_browser_version' => $browser->getVersion() ?? ''
            ,'client_os'              => $os->getName() ?? ''
            ,'client_os_version'      => $os->getVersion() ?? ''
            ,'client_device'          => $device->getName() ?? ''
        ];
        self::create($data);
    }

}