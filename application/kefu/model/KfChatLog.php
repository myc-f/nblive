<?php
namespace app\kefu\model;
use think\Model;
use app\kefu\model\KfUser as KfUserModel;
class KfChatLog extends Model
{
    protected $autoWriteTimestamp = true;
    /**
     * 聊天日志
     *
     * @param [type] $fd
     * @param [type] $data
     * @return void
     */
    public static function addData($fd,$param){

        $data = $param['data'];
        $source = $data['to']['source'];
        $addData=[
            'from_code'    => $data['mine']['code']
            ,'from_name'   => $data['mine']['name']
            ,'from_avatar' => $data['mine']['avatar']
            ,'to_code'     => $data['to']['code']
            ,'to_name'     => $data['to']['name']
            ,'content'     => $data['to']['content']
            ,'shop_id'     => $data['to']['shop_id']
            ,'source'      => $source
        ];
        if($source == 'kf'){
            $kf_online=KfUserModel::where(['code'=>$data['to']['code']])->value('online');
            $kf_online == 0 && $addData['status'] = 0;
        }
        self::create($addData);
    }
}