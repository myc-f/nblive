<?php
namespace app\kefu;

use app\common\controller\Common;

class MerchantInit extends Common
{

    public function __construct()
    {
        parent::__construct();
        if(!session('?merchant')){
            $this->redirect(url('login/index'));
        }
        $this->shop = session('merchant');
        $this->assign('socket', env('live.chat'));
        $this->assign('merchant',$this->shop);
    }
}
