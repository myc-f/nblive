<?php


namespace app\common\controller;
use View;
use think\Controller;
use think\facade\Env;

/**
 * 控制器基类
 * @package app\common\controller
 */
class Base extends Controller
{
    protected function initialize() {
        $this->assign('version',config('nblive.'));
    }

}
