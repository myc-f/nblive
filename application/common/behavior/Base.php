<?php
namespace app\common\behavior;
use Env;
use Request;
use View;

/**
 * 初始化基础配置行为
 * 将扩展的全局配置本地化
 */
class Base
{
    public function run()
    {

        // 获取当前模块名称
        $module = strtolower(Request::module());

        // 安装操作直接return
        if (defined('INSTALL_ENTRANCE')) {
            return;
        }

        // 获取站点根目录

        $rootDir = Request::rootUrl();
       
        define('ROOT_DIR', $rootDir);

        $theme = 'default';
        //defined('ENTRANCE') ? 'default/'.$module.'/'.ENTRANCE :'default';
        $viewReplaceStr = [
            // 站点根目录
            '__ROOT_DIR__' => $rootDir,
            // 静态资源根目录
            '__STATIC__' => $rootDir . 'static',
            // 文件上传目录
            '__UPLOAD__' => $rootDir . 'upload',

            // 前台公共静态目录
            '__PUBLIC_CSS__' => $rootDir . 'static/css',
            '__PUBLIC_JS__' => $rootDir . 'static/js',
            '__PUBLIC_IMG__' => $rootDir . 'static/image',

            //layadmin
            '__LAYADMIN__' => $rootDir . 'layadmin',
        ];

        View::config(['tpl_replace_string' => $viewReplaceStr]);

        $domain = Request::domain();
        $wap = config('base.wap_domain');
        $viewPath = 'theme/' . $theme . '/'. $module . '/';

        // 定义前台模板路径[分手机和PC]
        if (Request::isMobile() === true &&
            config('base.wap_site_status') &&
            file_exists('.' . ROOT_DIR . $viewPath . 'wap/')) {

            if ($wap && $wap != $domain) {
                header('Location: ' . $wap . Request::url());
                exit();
            }

            $viewPath .= 'wap/';
        } else if (config('base.wap_site_status') && $domain == $wap) {
            $viewPath .= 'wap/';
        }
        View::config(['view_path' => $viewPath]);
    }

}
