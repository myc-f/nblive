/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.2.100_3306
 Source Server Type    : MySQL
 Source Server Version : 50646
 Source Host           : 192.168.2.100:3306
 Source Schema         : nblive

 Target Server Type    : MySQL
 Target Server Version : 50646
 File Encoding         : 65001

 Date: 10/01/2020 18:41:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nb_kf_chat_log
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_chat_log`;
CREATE TABLE `nb_kf_chat_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT 0 COMMENT '店铺ID',
  `from_code` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '网页用户随机编号(仅为记录参考记录)',
  `from_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发送者名称',
  `from_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发送者头像',
  `to_code` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '接收方',
  `to_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '接受者名称',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发送的内容',
  `source` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '来源 ',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录时间',
  `status` tinyint(255) NOT NULL DEFAULT 1 COMMENT '0未读1已读',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fromid`(`from_code`(4)) USING BTREE,
  INDEX `toid`(`to_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '聊天记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for nb_kf_groups
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_groups`;
CREATE TABLE `nb_kf_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分组id',
  `shop_id` int(11) NOT NULL DEFAULT 0 COMMENT '店铺id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分组名称',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '分组状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '客服分组' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for nb_kf_guest_trace
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_guest_trace`;
CREATE TABLE `nb_kf_guest_trace`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '游客的uuid',
  `shop_id` int(11) NOT NULL DEFAULT 0 COMMENT '店铺ID',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '动作',
  `fd` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '游客ws的fd',
  `ua` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '浏览器UA',
  `ip` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '游客ip',
  `referer` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '游客referer',
  `talk_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '浏览页面',
  `source` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '来源域名',
  `client_browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '浏览器',
  `client_browser_version` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '浏览器版本号',
  `client_os` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客户端操作系统',
  `client_os_version` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作系统版本号',
  `client_device` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客户端设备',
  `created_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_created_time`(`created_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户跟踪' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for nb_kf_shop
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_shop`;
CREATE TABLE `nb_kf_shop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '店铺id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '店铺名称',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '唯一编码',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录名称',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录密码',
  `salt` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码混淆',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '店铺头像',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '到期时间(0永久)',
  `last_login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登陆IP',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登陆时间',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '店铺状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '店铺' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of nb_kf_shop
-- ----------------------------
INSERT INTO `nb_kf_shop` VALUES (1, '我的店铺', 'DF747636-A954-6B55-56C0-3557C6789B22', 'shop', '7ed233a10eafb8ee9d050395d18d5ead', 'b9c0f726a8', '/upload/1.jpg', 0, '192.168.2.222', 1578652073, 0, 1);

-- ----------------------------
-- Table structure for nb_kf_user
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_user`;
CREATE TABLE `nb_kf_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客服id',
  `shop_id` int(11) NOT NULL DEFAULT 0 COMMENT '店铺id',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '唯一编码',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服名称',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服登录密码',
  `salt` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码混淆',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服头像',
  `online` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否在线',
  `group_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属分组id',
  `fd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'fd',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '到期时间(0永久)',
  `last_login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登陆IP',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登陆时间',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `service_num` int(10) NOT NULL DEFAULT 0 COMMENT '正在服务用户数量',
  `sort` int(3) NOT NULL DEFAULT '100' COMMENT '优先级',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '用户状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user`(`username`, `shop_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '客服信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of nb_kf_user
-- ----------------------------
INSERT INTO `nb_kf_user` VALUES (1, 1, 'FE705D16-335A-DF3A-C692-7405356BE608', '客服小丽', '7a1eb65919045186b6ee50b6b60109cd', 'EJVki9LkZi', '/upload/9.jpg', 1, 0, '', 0, '192.168.2.222', 1578652297, 0, 0, 1);

-- ----------------------------
-- Table structure for nb_kf_words
-- ----------------------------
DROP TABLE IF EXISTS `nb_kf_words`;
CREATE TABLE `nb_kf_words`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT 0 COMMENT '店铺id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '常用语内容',
  `add_time` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `welcom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '常用语' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for nb_kf_service_log
-- ----------------------------
SET FOREIGN_KEY_CHECKS = 1;

DROP TABLE IF EXISTS `nb_kf_service_log`;
CREATE TABLE `nb_kf_service_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '店铺ID',
  `fd` varchar(50) NOT NULL COMMENT '通讯FD',
  `kf_code` varchar(55) NOT NULL DEFAULT '' COMMENT '网页用户随机编号(仅为记录参考记录)',
  `kf_name` varchar(100) NOT NULL DEFAULT '' COMMENT '发送者名称',
  `kf_avatar` varchar(100) NOT NULL DEFAULT '' COMMENT '发送者头像',
  `guest_code` varchar(55) NOT NULL DEFAULT '' COMMENT '接收方',
  `guest_name` varchar(100) NOT NULL DEFAULT '' COMMENT '接受者名称',
  `guest_avatar` varchar(100) NOT NULL COMMENT '发送的内容',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '服务开始时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '服务结束时间',
  `status` tinyint(255) NOT NULL DEFAULT '1' COMMENT '0结束1正在服务',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fromid` (`kf_code`(4)) USING BTREE,
  KEY `toid` (`guest_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='服务记录';