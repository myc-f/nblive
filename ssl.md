## centos ssl 本地证书生成

openssl req -x509 -nodes -days 1095 -newkey rsa:2048 -keyout *.web.key -out *.web.crt -config req.cnf -sha256
openssl x509 -inform der -in *.web.crt -out *.web.pem

Country Name (2 letter code) [XX]:CN
State or Province Name (full name) []:YunNan
Locality Name (eg, city) [Default City]:KunMing
Organization Name (eg, company) [Default Company Ltd]:nbnat
Organizational Unit Name (eg, section) []:ws
Common Name (eg, your name or your server's hostname) []:
Email Address []:vip@nbnat.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:abcd1234
An optional company name []:nbnat

1、yum install openssl安装openssl组件
2、生成KEY的流程步骤如下

 1. 创建根证书密钥文件(自己做CA)root.key：
 openssl genrsa -out root.key

输出内容为：

[lenin@archer ~]$ openssl genrsa -des3 -out root.key 
Generating RSA private key, 512 bit long modulus 
……………..++++++++++++ 
..++++++++++++ 
e is 65537 (0×10001) 


2. 创建根证书的申请文件root.csr：

openssl req -new -key root.key -out root.csr

输出内容为：

[lenin@archer ~]$ openssl req -new -key root.key -out root.csr 
You are about to be asked to enter information that will be incorporated 
into your certificate request. 
What you are about to enter is what is called a Distinguished Name or a DN. 
There are quite a few fields but you can leave some blank 
For some fields there will be a default value, 
If you enter ‘.’, the field will be left blank. 
—– 
Country Name (2 letter code) [AU]:CN ← 国家代号，中国输入CN 
State or Province Name (full name) [Some-State]:BeiJing ← 省的全名，拼音 
Locality Name (eg, city) []:BeiJing ← 市的全名，拼音 
Organization Name (eg, company) [Internet Widgits Pty Ltd]:MyCompany Corp. ← 公司英文名 
Organizational Unit Name (eg, section) []: ← 可以不输入 
Common Name (eg, YOUR name) []: ← 此时不输入 
Email Address []:admin@mycompany.com ← 电子邮箱，可随意填

Please enter the following ‘extra’ attributes 
to be sent with your certificate request 
A challenge password []: ← 可以不输入 

          An optional company name []: ← 可以不输入 

 3. 创建一个自当前日期起为期十年的根证书root.crt：

openssl x509 -req -days 3650 -sha1 -extensions v3_ca -signkey root.key -in root.csr -out root.crt

输出内容为：
[lenin@archer ~]$ openssl x509 -req -days 3650 -sha1 -extensions v3_ca -signkey root.key -in root.csr -out root.crt  

Getting Private key 
subject=/C=CN/ST=BeiJing/L=BeiJing/O=MyCompany Corp./emailAddress=admin@mycompany.com 
Signature ok 

4. 创建服务器证书密钥server.key：

openssl genrsa -out server.key 2048

输出内容为：

[lenin@archer ~]$ openssl genrsa -out server.key 2048 
Generating RSA private key, 2048 bit long modulus 
….+++ 
…………………………………………..+++ 
e is 65537 (0×10001)

去除key文件口令的命令: openssl rsa -in server.key -out server.key
运行时会提示输入密码,此密码用于加密key文件(参数des3便是指加密算法,当然也可以选用其他你认为安全的算法.),以后每当需读取此文件(通过openssl提供的命令或API)都需输入口令.如果觉得不方便,也可以去除这个口令,但一定要采取其他的保护措施! 

 5.创建服务器证书的申请文件server.csr：

openssl req -new -key server.key -out server.csr

输出内容为：

[lenin@archer ~]$ openssl req -new -key server.key -out server.req 
You are about to be asked to enter information that will be incorporated 
into your certificate request. 
What you are about to enter is what is called a Distinguished Name or a DN. 
There are quite a few fields but you can leave some blank 
For some fields there will be a default value, 
If you enter ‘.’, the field will be left blank. 
—– 
Country Name (2 letter code) [AU]:CN ← 国家名称，中国输入CN 
State or Province Name (full name) [Some-State]:BeiJing ← 省名，拼音 
Locality Name (eg, city) []:BeiJing ← 市名，拼音 
Organization Name (eg, company) [Internet Widgits Pty Ltd]:MyCompany Corp. ← 公司英文名 
Organizational Unit Name (eg, section) []: ← 可以不输入 
Common Name (eg, YOUR name) []:www.mycompany.com ← 服务器主机名，若填写不正确，浏览器会报告证书无效，但并不影响使用 
Email Address []:admin@mycompany.com ← 电子邮箱，可随便填

An optional company name []: ← 可以不输入
A challenge password []: ← 可以不输入 
to be sent with your certificate request 
Please enter the following ‘extra’ attributes 

6. 创建自当前日期起有效期为期十年的服务器证书server.crt：

openssl x509 -req -days 3650 -sha1 -extensions v3_req -CA root.crt -CAkey root.key -CAserial root.srl -CAcreateserial -in server.csr -out server.crt

输出内容为：
[lenin@archer ~]$ openssl x509 -req -days 3650 -sha1 -extensions v3_req -CA root.crt -CAkey root.key -CAserial root.srl -CAcreateserial -in server.csr -out server.crt

Getting CA Private Key 

subject=/C=CN/ST=BeiJing/L=BeiJing/O=MyCompany Corp./CN=www.mycompany.com/emailAddress=admin@mycompany.com 
Signature ok 

 

7. 创建客户端证书密钥文件client.key：

openssl genrsa -out client.key 2048

输出内容为：

[lenin@archer ~]$ openssl genrsa -out client.key 2048 

e is 65537 (0×10001) 
……………………………………………………………………………………………………….+++ 
……………………………………………………………………………..+++ 
Generating RSA private key, 2048 bit long modulus 

8. 创建客户端证书的申请文件client.csr：

openssl req -new -key client.key -out client.csr

输出内容为：

[lenin@archer ~]$ openssl req -new -key client.key -out client.csr 
Enter pass phrase for client.key: ← 输入上一步中创建的密码 
You are about to be asked to enter information that will be incorporated 
into your certificate request. 
What you are about to enter is what is called a Distinguished Name or a DN. 
There are quite a few fields but you can leave some blank 
For some fields there will be a default value, 
If you enter ‘.’, the field will be left blank. 
—– 
Country Name (2 letter code) [AU]:CN ← 国家名称，中国输入CN 
State or Province Name (full name) [Some-State]:BeiJing ← 省名称，拼音 
Locality Name (eg, city) []:BeiJing ← 市名称，拼音 
Organization Name (eg, company) [Internet Widgits Pty Ltd]:MyCompany Corp. ← 公司英文名 
Organizational Unit Name (eg, section) []: ← 可以不填 
Common Name (eg, YOUR name) []:Lenin ← 自己的英文名，可以随便填 
Email Address []:admin@mycompany.com ← 电子邮箱，可以随便填

 
An optional company name []: ← 可以不填
A challenge password []: ← 可以不填 
to be sent with your certificate request 

Please enter the following ‘extra’ attributes


9. 创建一个自当前日期起有效期为十年的客户端证书client.crt：

openssl x509 -req -days 3650 -sha1 -extensions v3_req -CA root.crt -CAkey root.key -CAserial root.srl -CAcreateserial -in client.csr -out client.crt

输出内容为：

[lenin@archer ~]$ openssl x509 -req -days 3650 -sha1 -extensions v3_req -CA root.crt -CAkey root.key -CAcreateserial -in client.csr -out client.crt 
Signature ok 
subject=/C=CN/ST=BeiJing/L=BeiJing/O=MyCompany Corp./CN=www.mycompany.com/emailAddress=admin@mycompany.com 
Getting CA Private Key 

10. 将客户端证书文件client.crt和客户端证书密钥文件client.key合并成客户端证书安装包client.pfx：

openssl pkcs12 -export -in client.crt -inkey client.key -out client.pfx

输出内容为：

[lenin@archer ~]$ openssl pkcs12 -export -in client.crt -inkey client.key -out client.pfx 
Enter Export Password: ← 输入一个新的密码，用作客户端证书的保护密码，在客户端安装证书时需要输入此密码 
Verifying – Enter Export Password: ← 确认密码，可以为空密码

11. 保存生成的文件备用，其中server.crt和server.key是配置单向SSL时需要使用的证书文件，client.crt是配置双向SSL时需要使用的证书文件，client.pfx是配置双向SSL时需要客户端安装的证书文件

.crt文件和.key可以合到一个文件里面，把2个文件合成了一个.pem文件（直接拷贝过去就行了）